#!/usr/bin/python
# -*- coding:utf-8 -*-

import MySQLdb
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

def Connect_DB():
    #Connect database lkl
    db = MySQLdb.Connect(host='10.1.11.41',
                         port=3306,
                         user='root',
                         passwd='123456',
                         db='lkl',
                         charset='utf8')
    cursor = db.cursor()

    #Creat table GeYan
    cursor.execute("DROP TABLE IF EXISTS GeYan") #If Table test already exists, ignore this statement

    sql_table = """CREATE TABLE GeYan (
                 id  int(20) NOT NULL ,
                 domain CHAR(20) CHARACTER SET utf8 DEFAULT NULL,
                 example CHAR(200) CHARACTER SET utf8 DEFAULT NULL,
                 example_length CHAR(20) NOT NULL,
                 obv_keyword  CHAR(20) CHARACTER SET utf8 DEFAULT NULL,
                 hid_keyword  CHAR(20) CHARACTER SET utf8 DEFAULT NULL)"""
    cursor.execute(sql_table)

    file_read = open("C:\Users\Administrator\Desktop\GY-DP.txt",'r')
    count = 0

    #write the processed text in advance to the database
    for line in file_read:
        # print type(line)
        count += 1
        if count == 1:
            continue
        line = line.strip().split('^')
        # print type(line)
        # print count
        # print line[0], line[1], line[2], line[3], line[4], line[5]
        try:
            cursor.execute("INSERT INTO GeYan(id, domain, example, example_length, obv_keyword, hid_keyword) values(%s, %s, %s, %s, %s, %s)",[line[0], line[1], line[2], line[3], line[4], line[5]])
            db.commit()   #Submit the results of the execution to the database, otherwise the import results will not appear
        except:
            db.rollback()
    file_read.close()
    cursor.close()
    db.close()

if __name__ == "__main__":
    Connect_DB()



#-------------------------------------
# 调试问题：

# F:/lkl-project/Python/Python-SQL/GY-DP-INSERT.py:42: Warning: Incorrect string value: '\xE5\xA4\xA9\xE6\xB0\x94...' for column 'example' at row 1
#   cursor.execute("INSERT INTO GeYan(id, domain, example, example_length, obv_keyword, hid_keyword) values(%s, %s, %s, %s, %s, %s)",[line[0], line[1], line[2], line[3], line[4], line[5]])
# <type 'unicode'>

# 问题分析：

# 出现这个错误的原因是，数据库的默认编码格式为latin1 而我要将utf8的中文插入到数据库中。

# 问题解答：

#     在使用SQL语句建立table是需要将字段的字符设置为与文本相同的编码格式  domain CHAR(20) NOT NULL,修改为domain CHAR(20) CHARACTER SET utf8 DEFAULT NULL，其他类似

#     sql_table = """CREATE TABLE test (
#                  id  int(20) NOT NULL ,
#                  domain CHAR(20) CHARACTER SET utf8 DEFAULT NULL,
#                  example CHAR(200) CHARACTER SET utf8 DEFAULT NULL,
#                  example_length CHAR(20) NOT NULL,
#                  obv_keyword  CHAR(20) CHARACTER SET utf8 DEFAULT NULL,
#                  hid_keyword  CHAR(20) CHARACTER SET utf8 DEFAULT NULL)"""

#-------------------------------------

