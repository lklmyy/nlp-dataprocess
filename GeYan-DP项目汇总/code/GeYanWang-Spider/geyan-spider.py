#coding:utf8
#written by python 2.7

import requests
from bs4 import BeautifulSoup
import sys
import re
reload(sys)
sys.setdefaultencoding('utf-8')

# 下载器，下载HTML内容
def Get_Html(url):
    # resp = requests.get(url)
    # return resp.content
    resp = requests.get(url, timeout = 20) #timeout参数是为了防止url不可访问，或者响应速度太慢而造成的时间浪费。
    resp.encoding= 'gb2312'
    print resp.text
    return resp.text

#解析器，解析HTML内容
def Parser_Html(html):
    out_text=open("C:\\Users\\Administrator\\Desktop\\1.txt",'w')
    soup = BeautifulSoup(html,'html.parser',from_encoding="utf-8")

    # 逐层寻找合适的标签
    body = soup.body
    geyan_middle = body.find('div',attrs={'id':'container'})  # find方法里面加上attrs参数，然后用字典传入想找的标签属性。
    geyan_list_ct = geyan_middle.find('div',attrs= {'class':'listbox'})

    geyan_list = []
    for geyan_ul in geyan_list_ct.find_all('ul',attrs={'class':'d1 ico3'}):

        # 获取格言网首页标题及对应URL
        for geyan_li in geyan_ul.find_all('li'):
            geyan_url = geyan_li.a['href']                  #获取网页中的url（使用字典的语法索引某个标签属性里面想要的值，如下为获取‘a'标签下herf属性的值）
            geyan_url_full =r"http://www.geyanw.com/%s"%geyan_url
            geyan_info = geyan_li.get_text()                #获取网页中的数据(使用get_text()方法获取想要标签里面的文字)
            geyan_list.append([geyan_info.encode('utf-8'), geyan_url_full.encode('utf-8')])

            inner_url = Get_Html(geyan_url_full)
            soup = BeautifulSoup(inner_url,'html.parser')

            title_node = soup.find('div', class_="title").find("h2")
            out_text.write(title_node.getText())
            out_text.write('\n')
            summary_node = soup.find('div', class_="content").find_all('p')
            for i in summary_node:
                if i!='\n':
                    # print(i.getText())
                    out_text.write(i.getText())
                    out_text.write('\n')
    out_text.close()

#输出器，打印爬虫结果
def recitify():
    f = open("C:\\Users\\Administrator\\Desktop\\1.txt", 'r')
    f1=open("C:\\Users\\Administrator\\Desktop\\2.txt", 'w')
    for line in f:
        line=re.sub(' ','',line)
        if len(line)>10:
            # print(line)
            f1.write(line)
    f.close()
    f1.close()
if __name__ == "__main__":
    URL = "http://www.geyanw.com/"
    html_ = Get_Html(URL)
    data_list = Parser_Html(html_)
    recitify()












