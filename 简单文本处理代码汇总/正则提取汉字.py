#coding=utf-8
# import re
#
# readfile = r"C:\Users\Administrator\Desktop\word.txt"
# writefile = r"C:\Users\Administrator\Desktop\word1.txt"
#
# with open(readfile,'r') as inputfile:
#     str=inputfile.read()
# outfile = open(writefile,'w')
# pattern=re.findall(r'\[u4e00-u9fa5\]',str)
# line_set=set()
# for line in pattern:
#     if line not in line_set:
#         outfile.write(line)
#         line_set.add(line)
# outfile.close()
# print('success！')
# # """
import re
def findPart(regex, text, name):
    res=re.findall(regex, text)
    if res:
        print "There are %d %s parts:\n"% (len(res), name)
        for r in res:
            print "\t",r
        print

readfile = r"C:\Users\Administrator\Desktop\Information3.txt"
writefile = r"C:\Users\Administrator\Desktop\Information4.txt"

with open(readfile, 'r') as inputfile:
    sample = inputfile.read()
outfile = open(writefile, 'w')
#sample is utf8 by default.
"""
sample='''en: Regular expression is a powerful tool for manipulating text.
zh: 正则表达式是一种很有用的处理文本的工具。
jp: 正規表現は非常に役に立つツールテキストを操作することです。
jp-char: あアいイうウえエおオ
kr:정규 표현식은 매우 유용한 도구 텍스트를 조작하는 것입니다.
puc: 。？！、，；：“ ”‘ ’——……·－·《》〈〉！￥％＆＊＃
'''
"""
#let's look its raw representation under the hood:
print "the raw utf8 string is:\n", repr(sample)


#find the non-ascii chars:
findPart(r"[\x80-\xff]+",sample,"non-ascii")

#convert the utf8 to unicode
usample=unicode(sample,'utf8')

#let's look its raw representation under the hood:
print "the raw unicode string is:\n", repr(usample)
print

#get each language parts:
findPart(u"[\u4e00-\u9fa5]+", usample, "unicode chinese")
findPart(u"[\uac00-\ud7ff]+", usample, "unicode korean")
findPart(u"[\u30a0-\u30ff]+", usample, "unicode japanese katakana")
findPart(u"[\u3040-\u309f]+", usample, "unicode japanese hiragana")
findPart(u"[\u3000-\u303f\ufb00-\ufffd]+", usample, "unicode cjk Punctuation")



"""
# https://lxneng.com/posts/88
#
# [转]Python 中文正则笔记
# Friday, July 9, 2010
# 总结在 python 语言里使用正则表达式匹配中文的经验。关键词：中文，cjk，utf8，unicode，python。
#
# 从字符串的角度来说，中文不如英文整齐、规范，这是不可避免的现实。本文结合网上资料以及个人经验，以 python
# 语言为例，稍作总结。欢迎补充或挑错。
#
# 一点经验
# 可以使用 repr()函数查看字串的原始格式。这对于写正则表达式有所帮助。
# Python 的 re模块有两个相似的函数：re.match(), re.search
# 。两个函数的匹配过程完全一致，只是起点不同。match只从字串的开始位置进行匹配，如果失败，它就此放弃；而search则会锲而不舍地完全遍历整个字串中所有可能的位置，直到成功地找到一个匹配，或者搜索完字串，以失败告终。如果你了解match的特性（在某些情况下比较快），大可以自由用它；如果不太清楚，search通常是你需要的那个函数。
# 从一堆文本中，找出所有可能的匹配，以列表的形式返回，这种情况用findall()这个函数。例子见后面的代码。
# utf8下，每个汉字占据3个字符位置，正则式为[\x80-\xff]{3}，这个都知道了吧。
#
# unicode下，汉字的格式如\uXXXX，只要找到对应的字符集的范围，就能匹配相应的字串，方便从多语言文本中挑出所需要的某种语言的文本。不过，对于像日文这样的粘着语，既有中文字符，又有平假名片假名，或许结果会有所偏差。
#
# 两种字符类可以并列在一起使用，例如，平假名、片假名、中文的放在一起，u”[\u4e00-\u9fa5\u3040-\u309f\u30a0-\u30ff]+”，来自定义所需要匹配的文本。
#
# 匹配中文时，正则表达式和目标字串的格式必须相同。这一点至关重要。或者都用默认的utf8，此时你不用额外做什么；如果是unicode，就需要在正则式之前加上u”"格式。
#
# 可以这样定义unicode字符串：string=u”我爱正则表达式”。如果字串不是unicode的，可以使用unicode()函数转换之。如果你知道源字串的编码，可以使用newstr=unicode(oldstring,
# original_coding_name)的方式转换，例如 linux 下常用unicode(string, “utf8″)，windows
# 下或许会用cp936吧，没测试。
# """
