#coding=utf-8
"""
提取出文本中的英文单词
"""
"""
step1、正则提取包含中文字符串
"""

"""
import re
with open(r'C:\Users\Administrator\Desktop\putao_dict1.txt') as file_source:
    str = file_source.read()
pattern_result=re.findall(r'("def":\[.*?\])|("trans":".*?")|("text":".*?")|("en":".*?")|("scode":".*?")|("k":".*?")|("fs":".*?")|(	.*?	)',str )
get_file_object = open(r'C:\Users\Administrator\Desktop\1_Eng.txt', "w")
for line in pattern_result:
    get_file_object.write("%s,%s,%s,%s,%s,%s,%s,%s\n" % (line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7]))
get_file_object.close()
"""


"""
step2、删除非关键字符

import re
with open(r'C:\Users\Administrator\Desktop\1_Eng.txt', "r")as file_source:
    lines=file_source.read()
#get_file_object = open(r'C:\Users\Administrator\Desktop\4.txt', "w")
get_file_object = open(r'C:\Users\Administrator\Desktop\1-1.txt', "w")
pattern_result = re.findall(r'(:".*")|(:\[".*"\])', lines)
for line in pattern_result:
    get_file_object.write("%s,%s\n" % (line[0], line[1]))
    #get_file_object.write('\n')
get_file_object.close()

"""

"""
step3、删除文本中特定的字符
"""
"""
"""
"""
"""
import re
file_source = open(r'C:\Users\Administrator\Desktop\1-9.txt','r')
s = file_source .read()
res = ''
#for i in re.split(r'\[|[A-Za-z0-9]|；|,|\)|\(|>|<|&#|β|\'|、|\...|\.|-|\\|;', s):
#for i in re.split(r'[0-9]|[\u4e00-\u9fa5]', s):
#for i in re.split(r',| |\'s|\|:|"|:|〔|〕|\(|\)|\'|）|（|，|”|“|……|\?', s):
for i in re.split(r'/|\\|', s):
    res += i+'\n'
    if len(res)<1:
      res=res.split()
file_source .close()
file_source = open(r'C:\Users\Administrator\Desktop\1-10.txt','w')
file_source .write(res)
file_source .close()


"""
file_source = open(r'C:\Users\Administrator\Desktop\text.txt','r')
data = file_source .read()
#english_only = ''.join(x for x in data if ord(x) < 256)
english_only =''.join([i if ord(i) < 128 else ' ' for i in data])
file_source .close()
file_source1 = open(r'C:\Users\Administrator\Desktop\text-rseult.txt','w')
file_source1 .write(english_only)
file_source1 .close()
"""

"""
step4、删除文件中的空白行
"""

"""
def delblankline(infile, outfile):
    infopen = open(infile, 'r')
    outfopen = open(outfile, 'w')
    lines = infopen.readlines()
    for line in lines:
        if line.split():
            outfopen.writelines(line)
        else:
            outfopen.writelines("")
    infopen.close()
    outfopen.close()


delblankline("7.txt", "8.txt")
"""

"""
step5、移除重复行
"""

"""
lines_seen = set()
outfile = open(r"C:\Users\Administrator\Desktop\9.txt", "w")
for line in open(r"8.txt", "r"):
 if line not in lines_seen:
    outfile.write(line)
    lines_seen.add(line)
outfile.close()
"""

"""
lines_seen = set()
outfile = open(r"C:\Users\Administrator\Desktop\11.txt", "w")
for line in open(r"C:\Users\Administrator\Desktop\10.txt", "r"):
 if line not in lines_seen:
    if len(line) < 1:
      line =line.strip()
      continue
    outfile.write(line)
    lines_seen.add(line)
outfile.close()
"""



"""
step6、提取指定长度汉字
"""

"""
file_source=open(r'C:\Users\Administrator\Desktop\2_ec_China4.txt','r')
file_object=open(r'C:\Users\Administrator\Desktop\2_ec_China5.txt', "w")
array=''
for line in file_source:
        line=line.strip()
        if 12<len(line):       #ec.txt 左开右闭3,6,9，12
             array=line
             file_object.write(array)
             file_object.write('\n')
file_object.close()
"""



"""
step7、分词
"""
"""
# !/usr/bin/env python 3
# encoding: utf-8
"""
"""
参考程序
"""
"""
程序名称: 依次迭代等间隔分词
程序功能：对文本通过迭代的方式依次分割成相同数量的字组合
开发平台：PyCharm Community Edition 2017.1.3
软件版本：Python3.6.1
开始日期：2017-6-25
结束日期：2016-6-25
学习内容：1、正则表达式（注意同一个字符不同的编码格式）
有待改善：1、编码问题：gbk，utf-8,ascii之间的关系转换
作    者：李开亮

"""
"""
def split_line(line, n):    #分解行
    if len(line) <= n:
        return line
    else:
        s = ""      #创建一个字符串
        for i in range(len(line) - n + 1):   #不理解它的作用
            s += line[i:i + n]  #切片
            s += ";"
        s += "\n"
        return s

if __name__ == '__main__':  #什么意思
    # 词组长度
    seq_len = 2
    # 读取文件路径
    path = "C:/Users/Administrator/Desktop/111.txt"
    file = open(path, 'r', encoding='utf-8')
    # 保存结果路径
    result_path = "C:/Users/Administrator/Desktop/1_2_result.txt"
    result_file = open(result_path, 'w', encoding='utf-8')
    for line in file:
        temp = line.strip()  #Python strip() 方法用于移除字符串头尾指定的字符（默认为空格）。
        result_file.writelines(split_line(temp, seq_len))  #writelines() 方法用于向文件中写入一序列的字符串
    # 关闭文件
    file.close()
    result_file.close()
"""


"""
step8、字符转换并换行输出
"""

"""
# !/usr/bin/env python 3
# encoding: utf-8
import re
file_source = open(r'C:\Users\Administrator\Desktop\1_2_result.txt','rb')
s = file_source .read().decode('utf-8')
file_source1 = open(r'F:\ciyu\two_word.txt','bw')
res = ''
for i in re.split(r';',s):
    res += i + '\n'
    file_source1.write(res.encode('utf-8'))
file_source .close()
file_source1 .close()
"""


"""
step9、删除重复的行
"""

"""
lines_seen = set()
outfile = open(r"C:\Users\Administrator\Desktop\114.txt", "w")
for line in open(r"C:\Users\Administrator\Desktop\113.txt", "r"):
 if line not in lines_seen:
    if len(line) < 1:
      line =line.strip()
      continue
    outfile.write(line)
    lines_seen.add(line)
outfile.close()

"""















