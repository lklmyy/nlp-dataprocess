#coding:utf-8
"""
开发程序
"""
"""
程序名称：去重删除空白行并去特定长度字符串
程序功能：删除重复的行,删除空白行,提取特定长度的中文字符串
开发平台：PyCharm Community Edition 2017.1.3
软件版本：Python2.7.13
开始日期：2017-6-2
结束日期：2017-6-3
学习内容：1、每个字符所占字节数；2、set(),open(),strip(),len()等方法的使用；
作   者：李开亮
"""
readDir = r"C:\Users\Administrator\Desktop\key-words1.txt"
writeDir = r"C:\Users\Administrator\Desktop\key-words2.txt"
lines_seen = set() #set()方法用于对文件去重
outfile=open(writeDir,"w")
f = open(readDir,"r")
for line in f:
    if 2<len(line):
        if line not in lines_seen:  #测试 line 是否不是 lines_seen的成员
            outfile.write(line)
            #outfile.write('\n')
            lines_seen.add(line)
outfile.close()
print ("success")