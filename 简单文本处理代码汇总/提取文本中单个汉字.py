# -*- coding: utf-8 -*-
"""
参考程序
"""
"""
Created on Fri Jan  6 11:17:32 2017
@author: yang
程序描述：提取汉字，计算重复
开发平台：PyCharm Community Edition 2017.1.3
软件版本：Python2.7.14
开始日期：2017-7-1
结束日期：2017-7
存在问题：1、无法解决由于空格键存在导致的重复问题（空格键的存在导致字符串长度不一）
有待改善：1、程序
"""

fid = open(r'C:\Users\Administrator\Desktop\single1.txt')
fidw = open(r'C:\Users\Administrator\Desktop\single2.txt','w')
lines = fid.readlines()
character = {}
exclude = ['','','”','“','，','：','；','）','（', '《','》','～','【','】','、','。','①','③','②','④','⑤','⑥','⑦','⑧','⑨']
for line in lines:
    line = line.strip()
    temp = ''
    for i in range(len(line)):
        if ord(line[i]) > 127:
            temp=temp+line[i]
    line = temp
    size = len(line)/3
    #print line,len(line)
    if len(line)%3 != 0:
        continue
    for i in range(size):
        c = line[i*3:i*3+3]
        #print c,len(c)
        if c not in character.keys():
            character[c]=1
        else:
            character[c]+=1
for key in character.keys():
    #print key, character[key]
    if key in exclude:
        continue
    fidw.writelines(key+' '+str(character[key])+'\n')
fid.close()
fidw.close()