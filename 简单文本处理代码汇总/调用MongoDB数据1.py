# -*- coding: UTF-8 -*-
"""
说明
"""
"""
问题描述：数据库中文件可显示到控制台，但无法写入文本文档
解决问题：解决Python2.7的UnicodeEncodeError: ‘ascii’ codec can’t encode异常错误
问题核心：编码问题（utf-8与ASCII）
开发平台：PyCharm Community Edition 2017.1.3
软件版本：Python2.7.13
日    期：2017-7-9
学习内容：1、
"""
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from pymongo import MongoClient
mongo = MongoClient('mongodb://10.1.11.17:27017/')
db = mongo['Sina_']
collection = db['Comments']

file=open(r'C:\Users\Administrator\Desktop\sina3.txt', 'w')
for i in collection.find({},{"commment_content":1,"_id":0}):
    if i.has_key("commment_content") and i["commment_content"]:
        n=len(i["commment_content"])
        for j in range(0,n):
            file.write(i["commment_content"][j])
            #file.write("\n")
file.close()
"""
UnicodeEncodeError：


Traceback (most recent call last):
  File "makedb.py", line 33, in 
    main()
  File "makedb.py", line 30, in main
    fp.write(row[1])
UnicodeEncodeError: 'ascii' codec can't encode characters in position 0-78: ordinal not in range(128)
本来以为数据读取错误，我特将fp.write改成print，结果数据全部读取并显示在命令控制台上了，证明代码是没有问题的，仔细看了下异常信息，
貌似是因为编码问题：Unicode编码与ASCII编码的不兼容，其实这个Python脚本文件是由utf-8编码的，同时SQlite3数据库存取的也是UTF-8格式，
Python默认环境编码通过下面的方法可以获取：

import sys
print sys.getdefaultencoding()
# 'ascii'
基本上是ascii编码方式，由此Python自然调用ascii编码解码程序去处理字符流，当字符流不属于ascii范围内，就会抛出异常（ordinal not in range(128)）。

解决的方案很简单，修改默认的编码模式，很多朋友会想到setdefaultencoding，是的，我们可以通过sys.setdefaultencoding(‘utf-8’)来将当前的字符处
理模式修改为utf-8编码模式，值得注意的是，如果单纯这么调用的话，Python会抛出一个AttributeError异常：

Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'module' object has no attribute 'setdefaultencoding'
竟然说sys没有setdefaultencoding的方法，其实sys是有这个方法的，但是要请出她老人家需要调用一次reload(sys)，很奇怪，是么？如果有谁知道原因的话，还望不吝赐教。

import sys
reload(sys)
sys.setdefaultencoding('utf-8')
好了，通过上面短短的三行，我们算是很好的解决了这个问题了，同样的方式也可以应用到UnicodeDecodeError上。
当然这个技巧来自于网络，我还找到其他特别的办法，但是感觉还是这个比较靠谱，有童鞋说：我们将Python 2.x系列升级
到Python 3.x系列就可以了，小小的问题犯不着升级吧，毕竟2到3还是要有个过渡的。
"""
