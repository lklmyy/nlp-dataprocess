# !/usr/bin/env python 3
# encoding: utf-8

"""
参考程序
"""
"""
程序名称: 依次迭代等间隔分词
程序功能：对文本通过迭代的方式依次分割成相同数量的字组合
开发平台：PyCharm Community Edition 2017.1.3
软件版本：Python3.6.1
开始日期：2017-6-25
结束日期：2016-6-25
学习内容：1、正则表达式（注意同一个字符不同的编码格式）
有待改善：1、编码问题：gbk，utf-8,ascii之间的关系转换
作    者：李开亮

"""
"""
"""
def split_line(line, n):    #分解行
    if len(line) <= n:
        return line
    else:
        s = ""      #创建一个字符串
        for i in range(len(line) - n + 1):   #不理解它的作用
            s += line[i:i + n]  #切片
            s += ";"
        s += "\n"
        return s

if __name__ == '__main__':  #什么意思
    # 词组长度
    seq_len = 10
    # 读取文件路径
    path = "C:/Users/Administrator/Desktop/2.txt"
    file = open(path, 'r', encoding='utf-8')
    # 保存结果路径
    result_path = "C:/Users/Administrator/Desktop/10_result.txt"
    result_file = open(result_path, 'w', encoding='utf-8')
    for line in file:
        temp = line.strip()  #Python strip() 方法用于移除字符串头尾指定的字符（默认为空格）。
        result_file.writelines(split_line(temp, seq_len))  #writelines() 方法用于向文件中写入一序列的字符串
    # 关闭文件
    file.close()
    result_file.close()